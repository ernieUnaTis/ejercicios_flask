# Ejercicio Softek



## Tecnologías utilizadas

- [ ] Python 3.9
- [ ] Flask
- [ ] Docker

## Descripción del proyecto

Entregar una API con tres endpoints para resolver los siguientes ejercicios-

- [ ] Custom Order Status
- [ ] Season Problems
- [ ] Detecting Change

## Como iniciar el proyecto

1. Clonar el proyecto
2. Dentro del proyecto, docker build . -t ejercicios
3. docker run -d -p 5000:5000 ejercicios ( Se ejecutara en el puerto 500)

## Como ejecutar el proyecto

Los tres endpoints son los siguientes:

1. http://127.0.0.1:5000/services/custom_order_status

2. http://127.0.0.1:5000/services/season_problems

3.- http://127.0.0.1:5000/services/detecting_change

Cada uno se ejecuta con una llamada POST y dara salida un dataframe en html ( una tabla)

No se necesita enviar parametros, ya están precargados por el ejercicio

Se adjunta colección de Insomnia para probar