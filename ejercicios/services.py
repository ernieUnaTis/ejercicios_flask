from flask import Blueprint,Response, request
from os import getenv
import numpy as np
import pandas as pd
from datetime import datetime
import json


services = Blueprint("services", __name__)

def get_status(row):
	if 'PENDING' in row.status:
		return "PENDING"
	if "SHIPPED" in row.status:
		return "SHIPPED"
	return "CANCELLED"

@services.route("/custom_order_status", methods=['GET', 'POST'])
def custom_order_status():
    #Creamos DataSEt
    df = pd.DataFrame({
	"order_number": ["ORD_1567","ORD_1567","ORD_1567","ORD_1234","ORD_1234","ORD_1234","ORD_9834","ORD_9834","ORD_7654","ORD_7654"],
        "item_name": ["LAPTOP","MOUSE","KEYBOARD","GAME","BOOK","BOOK","SHIRT","PANTS","TV","DVD"],
        "status": ["SHIPPED","SHIPPED","PENDING","SHIPPED","CANCELLED","CANCELLED","SHIPPED","CANCELLED","CANCELLED","CANCELLED"]
    }
    )
    df2 = df[["order_number", "status"]]
    #Agrupamos por order_number y en un set juntamos los status
    df3 = df2.groupby("order_number")['status'].apply(set).reset_index(name='status')
    #Validamos las reglas de estatus
    df3["status"] = df3.apply(get_status,axis=1)
    result = df3.to_html(index = False)
    return result

def get_season(row):
	date_split = row.ORD_DT.split("/")
	date_time_obj = datetime.strptime(row.ORD_DT, '%m/%d/%y')
	print(date_time_obj)

	if(date_time_obj > datetime.strptime("3/19/"+str(date_split[2]), '%m/%d/%y') and date_time_obj < datetime.strptime("6/19/"+str(date_split[2]), '%m/%d/%y')):
		return  "Spring"
	elif (date_time_obj > datetime.strptime("6/20/"+str(date_split[2]), '%m/%d/%y') and date_time_obj < datetime.strptime("9/21/"+str(date_split[2]), '%m/%d/%y')):
		return "Summer"
	elif (date_time_obj > datetime.strptime("9/22/"+str(date_split[2]), '%m/%d/%y') and date_time_obj < datetime.strptime("12/20/"+str(date_split[2]), '%m/%d/%y')):
		return "Fall"
	else:
		return "Winter"
	end

@services.route("/season_problems", methods=['GET', 'POST'])
def season_problems():
    df = pd.DataFrame({
	"ORD_ID": ["113","114","115","116","117","118","119","120"],
        "ORD_DT": ["9/23/19","1/1/20","12/5/19","9/24/20","1/30/20","5/2/20","4/2/20","10/9/20"],
        "QT_ORDD": [1,1,1,1,1,1,1,1]
    }
    )
    df["season"] = df.apply(get_season,axis=1)
    result = df[["ORD_ID","season"]].to_html(index = False)
    return result


def comp_prev(row):
    if(row.was_rainy == True and row.prev_a == False):
    	return True
    else:
    	return False

@services.route("/detecting_change", methods=['GET', 'POST'])
def detecting_change():
    df = pd.DataFrame({
        "date": ["1/1/20","1/2/20","1/3/20","1/4/20","1/5/20","1/6/20","1/7/20","1/8/20","1/9/20","1/10/20"],
            "was_rainy": [False,True,True,False,False,True,False,True,True,True],

        }
    )
    df['prev_a'] = df['was_rainy'].shift()
    df['became_bad']= df.apply(comp_prev,axis=1)
    df2 = df.query('became_bad == True')
    result = df2[["date","was_rainy"]].to_html(index = False)
    return result