from flask import Flask,request, jsonify, Response
from flask_cors import CORS
from ejercicios.services import services

from flask import Flask
app = Flask(__name__)
CORS(app)
# Register blueprints
app.register_blueprint(services, url_prefix="/services")

@app.route('/')
def hello_world():
    return 'Hello, World!'


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
